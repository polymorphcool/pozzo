#!/usr/bin/env python3

import os, shutil, sys, copy

# GLOBALS
GODOT_FOLDER = 'godot'
THEME_FOLDER = 'theme'

# default configuration
line_length = 80
config = {
	'update_git': True,
	'jobs': 7,
	'width': 1280,
	'height': 720,
	'versions': [
		{
			'enabled': False,
			'name': '3.3', 
			'address': 'git@github.com:godotengine/godot.git',
			'remote':'origin 3.3',
			'tag': '3.3.2-stable',
			'replace': [
				# naming
				['methods.py', 'godot', 'pozzo'],
				['version.py', 'short_name = "godot"', 'short_name = "pozzo"'],
				['version.py', 'name = "Godot Engine"', 'name = "POZZO engine"'],
				['version.py', 'website = "https://godotengine.org"', 'website = "https://polymorphcool.itch.io/pozzo"'],
				['platform/windows/godot_res.rc', "Godot Engine", "POZZO engine"],
				['platform/x11/context_gl_x11.cpp', "Godot Engine", "POZZO engine"],
				['platform/x11/context_gl_x11.cpp', "Godot Engine", "POZZO engine"],
				# colors
				['editor/editor_themes.cpp', 'preset_accent_color = Color(0.41, 0.61, 0.91);', 'preset_accent_color = Color(0., 1., 0.93);'],
				['editor/editor_themes.cpp', 'preset_base_color = Color(0.2, 0.23, 0.31);', 'preset_base_color = Color(0., 0., 0.);'],
				['editor/editor_fonts.cpp', '(_font_NotoSansUI_Regular, _font_NotoSansUI_Regular_size);', '(_font_Hack_Regular, _font_Hack_Regular_size);'],
				['editor/editor_fonts.cpp', '(_font_NotoSansUI_Bold, _font_NotoSansUI_Bold_size);', '(_font_Hack_Bold, _font_Hack_Bold_size);'],
				['editor/editor_settings.cpp', 'Color(0.2, 0.23, 0.31)', 'Color(0.2, 0.2, 0.3)'],
				['editor/editor_settings.cpp', 'Color(0.41, 0.61, 0.91)', 'Color(0.2, 0.91, 0.91)'],
				['main/main_builders.py', 'Color(0.14, 0.14, 0.14)', 'Color(0, 0, 0)'],
				['main/main_builders.py', 'Color(0.125, 0.145, 0.192)', 'Color(0, 0, 0)'],
				['main/splash.gen.h', 'Color(0.14, 0.14, 0.14)', 'Color(0, 0, 0)'],
				['main/main.cpp', 'GLOBAL_DEF("rendering/environment/default_clear_color", Color(0.3, 0.3, 0.3))', 'GLOBAL_DEF("rendering/environment/default_clear_color", Color(0, 0, 0))'],
				# default size
				['editor/editor_node.cpp', 'Size2(1024, 600)', 'Size2(%pozzo_width%, %pozzo_height%)'],
				['main/main.cpp', 'GLOBAL_DEF("display/window/size/width", 1024);', 'GLOBAL_DEF("display/window/size/width", %pozzo_width%);'],
				['main/main.cpp', 'GLOBAL_DEF("display/window/size/height", 600);', 'GLOBAL_DEF("display/window/size/height", %pozzo_height%);'],
				['core/os/os.h', 'int p_width = 1024, int p_height = 600', 'int p_width = %pozzo_width%, int p_height = %pozzo_height%']
				# custom
				#['drivers/gles3/rasterizer_scene_gles3.cpp', 'glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);', 'glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);']
			]
		},
		{
			'enabled': True,
			'name': '3.4', 
			'address': 'git@github.com:godotengine/godot.git',
			'remote':'origin 3.4',
			'tag': '3.4.2-stable',
			'replace': [
				# naming
				['methods.py', 'godot', 'pozzo'],
				['version.py', 'short_name = "godot"', 'short_name = "pozzo"'],
				['version.py', 'name = "Godot Engine"', 'name = "POZZO engine"'],
				['version.py', 'website = "https://godotengine.org"', 'website = "https://polymorphcool.itch.io/pozzo"'],
				['platform/windows/godot_res.rc', "Godot Engine", "POZZO engine"],
				['platform/x11/context_gl_x11.cpp', "Godot Engine", "POZZO engine"],
				['platform/x11/context_gl_x11.cpp', "Godot Engine", "POZZO engine"],
				# colors
				['editor/editor_themes.cpp', 'preset_accent_color = Color(0.41, 0.61, 0.91);', 'preset_accent_color = Color(0., 1., 0.93);'],
				['editor/editor_themes.cpp', 'preset_base_color = Color(0.2, 0.23, 0.31);', 'preset_base_color = Color(0., 0., 0.);'],
				#['editor/editor_fonts.cpp', '(_font_NotoSansUI_Regular, _font_NotoSansUI_Regular_size);', '(_font_Hack_Regular, _font_Hack_Regular_size);'],
				#['editor/editor_fonts.cpp', '(_font_NotoSansUI_Bold, _font_NotoSansUI_Bold_size);', '(_font_Hack_Bold, _font_Hack_Bold_size);'],
				['editor/editor_settings.cpp', 'Color(0.2, 0.23, 0.31)', 'Color(0.2, 0.2, 0.3)'],
				['editor/editor_settings.cpp', 'Color(0.41, 0.61, 0.91)', 'Color(0.2, 0.91, 0.91)'],
				['main/main_builders.py', 'Color(0.14, 0.14, 0.14)', 'Color(0, 0, 0)'],
				['main/main_builders.py', 'Color(0.125, 0.145, 0.192)', 'Color(0, 0, 0)'],
				['main/splash.gen.h', 'Color(0.14, 0.14, 0.14)', 'Color(0, 0, 0)'],
				['main/main.cpp', 'GLOBAL_DEF("rendering/environment/default_clear_color", Color(0.3, 0.3, 0.3))', 'GLOBAL_DEF("rendering/environment/default_clear_color", Color(0, 0, 0))'],
				# default size
				['editor/editor_node.cpp', 'Size2(1024, 600)', 'Size2(%pozzo_width%, %pozzo_height%)'],
				['main/main.cpp', 'GLOBAL_DEF("display/window/size/width", 1024);', 'GLOBAL_DEF("display/window/size/width", %pozzo_width%);'],
				['main/main.cpp', 'GLOBAL_DEF("display/window/size/height", 600);', 'GLOBAL_DEF("display/window/size/height", %pozzo_height%);'],
				['core/os/os.h', 'int p_width = 1024, int p_height = 600', 'int p_width = %pozzo_width%, int p_height = %pozzo_height%']
				# custom
				#['drivers/gles3/rasterizer_scene_gles3.cpp', 'glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);', 'glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);']
			]
		},
		{ 
			'enabled': False,
			'name': '4.0',
			'address': 'git@github.com:godotengine/godot.git',
			'remote':'origin master',
			'tag': None
		}
	],
	'modules': { 
		'3.3': [
			{ 
				'name': 'boids', 
				'address': 'git@gitlab.com:polymorphcool/godot_module_boids.git',
				'remote':'origin master',
				'tag': None, 
				'enabled': True, 
				'os':['windows','osx','linuxbsd'] 
			},
			{ 
				'name': 'gosc', 
				'address': 'git@gitlab.com:polymorphcool/gosc.git',
				'remote':'origin master',
				'tag': None,
				'enabled': True,
				'os':['windows','osx','linuxbsd'] 
			},
			{ 
				'name': 'ffmpeg', 
				'address': 'git@gitlab.com:polymorphcool/godot-ffmpeg.git',
				'remote':'origin master',
				'tag': None,
				'enabled': True,
				'os':['windows','linuxbsd'] 
			}
			],
		'3.4': [
			{ 
				'name': 'boids', 
				'address': 'git@gitlab.com:polymorphcool/godot_module_boids.git',
				'remote':'origin master',
				'tag': None, 
				'enabled': True, 
				'os':['windows','osx','linuxbsd'] 
			},
			{ 
				'name': 'gosc', 
				'address': 'git@gitlab.com:polymorphcool/gosc.git',
				'remote':'origin master',
				'tag': None,
				'enabled': True,
				'os':['windows','osx','linuxbsd'] 
			},
			{ 
				'name': 'ffmpeg', 
				'address': 'git@gitlab.com:polymorphcool/godot-ffmpeg.git',
				'remote':'origin master',
				'tag': None,
				'enabled': True,
				'os':['windows','linuxbsd'] 
			}
			],
		'4.0': [],
		'all': [
			{ 
				'name': 'boids', 
				'address': 'git@gitlab.com:polymorphcool/godot_module_boids.git',
				'remote':'origin master', 	
				'tag': None,
				'enabled': True,
				'os':['windows','osx','linuxbsd'] 
			},
			{ 
				'name': 'futari', 
				'address': 'git@gitlab.com:polymorphcool/futari-addon.git',
				'remote':'origin master',
				'tag': None,
				'enabled': False,
				'os':['windows','osx','linuxbsd']
			},
			{ 
				'name': 'softskin', 
				'address': 'git@gitlab.com:frankiezafe/SoftSkin.git',
				'remote':'origin master', 	
				'tag': None,  
				'enabled': False, 
				'os':['windows','osx','linuxbsd']
			},
			{ 
				'name': 'slicer', 
				'address': 'git@github.com:cj-dimaggio/godot-slicer.git',
				'remote':'origin master',
				'tag': None,
				'enabled': False,
				'os':['windows','osx','linuxbsd'] 
			},
			{ 
				'name': 'gosc', 
				'address': 'git@gitlab.com:polymorphcool/gosc.git',
				'remote':'origin master',
				'tag': None,
				'enabled': True,
				'os':['windows','osx','linuxbsd'] 
			},
			{ 
				'name': 'gsyphon', 
				'address': 'git@gitlab.com:polymorphcool/gsyphon.git',
				'remote':'origin master',
				'tag': None,
				'enabled': False,
				'os':['osx']
			},
			{ 
				'name': 'ffmpeg', 
				'address': 'git@gitlab.com:polymorphcool/godot-ffmpeg.git',
				'remote':'origin master',
				'tag': None,
				'enabled': True,
				'os':['windows','linuxbsd'] 
			}
		] 
	},
	'targets': [
		{ 
			'enabled': False,
			'clear': False,
			'target':'debug',
			'tools': False,
			'bits': 32
		},
		{ 
			'enabled': False,
			'clear': False,
			'target':'release_debug',
			'tools': False,
			'bits': 32
		},
		{ 
			'enabled': False,
			'clear': False,
			'target':'release',
			'tools': True,
			'bits': 32
		},
		{ 
			'enabled': True,
			'clear': False,
			'target':'debug',
			'tools': True,
			'bits': 64
		},
		{ 
			'enabled': True,
			'clear': False,
			'target':'release_debug',
			'tools': False,
			'bits': 64
		},
		{ 
			'enabled': True,
			'clear': False,
			'target':'release',
			'tools': False,
			'bits': 64
		}
	],
	'platforms': [
		{ 
			'enabled': True,
			'clear': False,
			'name': 'linuxbsd',
			'llvm': False,
			'lld': False
		},
		{ 
			'enabled': True,
			'clear': False,
			'name': 'windows',
			'llvm': False,
			'lld': False
		},
		{ 
			'enabled': False,
			'clear': False,
			'name': 'osx',
			'llvm': False,
			'lld': False
		},
		{ 
			'enabled': False,
			'clear': False,
			'name': 'raspbian',
			'llvm': False,
			'lld': False
		}
	]
}
curr_config = None
block_separator = ''
edit = None
cwd = os.getcwd()

def wrap_title( s, center = False ):
	out = ''
	if center:
		out += ' ' + s + ' '
		before = int( (line_length - len(out)) / 2 )
		if before < 0:
			before = 0
		after = before
		while len(out) + before + after < line_length:
			after += 1
		for i in range(0,before):
			out = '\\'+ out
		for i in range(0,before):
			out = out + '/'
	else:
		out += '# ' + s + ' '
		while len( out ) < line_length:
			out += '#'
	return out

def print_header():
	global block_separator
	while len( block_separator ) < line_length:
		block_separator += '#'
	print(block_separator)
	print( wrap_title('POZZO compilation', True) )
	print(block_separator)

def print_clist( id, key, compact = False ):
	
	value = config[key]
	if not type(value) is list:
		return
	print( wrap_title(str(id)+'.'+key) )
	
	# separator
	sep = ''
	while len( sep ) < line_length:
		sep += '-'
	# loading list
	columns = []
	cells = []
	enables = []
	vi = 0
	for v in value:
		ki = 0
		c = []
		for k in v:
			if compact and k == 'id':
				continue
			vv = v[k]
			if type(vv) is bool:
				if len(enables) <= vi:
					while len(enables) <= vi:
						enables.append(False)
					enables[vi] = vv
				continue
			append_header = False
			while len(columns) <= ki:
				columns.append(0)
				append_header = True
			if append_header:
				if len(cells) == 0:
					cells.append([])
				cells[0].append(k)
			if columns[ki] < len(k):
				columns[ki] = len(k)
			sv = '' 
			if type(vv) is list:
				for vvv in vv:
					if len(sv) != 0:
						sv += ','
					sv += str(vvv)
			else:
				sv = str(vv)
			c.append( sv )
			if columns[ki] < len(sv):
				columns[ki] = len(sv)
			ki += 1
		if len(c) > 0:
			cells.append(c)
		vi += 1
	# build table
	if len(cells) > 1:
		columns[0] += 2
		line = ''
		if not compact:
			for i in range(0,len(columns)):
				columns[i] += 1
				word = cells[0][i]
				if i == 0:
					word = '  ' + word
				while len(word) < columns[i]:
					word += ' '
				line += word
			print(line)
		print(sep)
		for ci in range(1,len(cells)):
			line = ''
			for i in range(0,len(columns)):
				word = cells[ci][i] + ' '
				if i == 0:
					if enables[ci-1]:
						word = '* ' + word
					else:
						word = '  ' + word
				while len(word) < columns[i]:
					word += ' '
				line += word
			print(line)
	print('')

def print_config( conf ):
	print( '\ncurrent configuration:\n' )
	ki = 0
	for k in conf:
		if type(conf[k]) is list:
			print_clist( ki, k, True )
		else:
			print( '#', str(ki)+'.'+k+':', conf[k], '\n' )
		ki += 1

def editor():
	print( "actions:")
	print( "\t'e': edit configuration")
	print( "\t'd': default configuration")
	print( "\t'r': reload last configuration")
	print( "\tleave blank to launch compilation" )
	data = input( "action:" )
	if data == '':
		return

def find_replace( cfg, path, old, new ):
	
	new = new.replace( '%pozzo_width%', str(cfg['width']) )
	new = new.replace( '%pozzo_height%', str(cfg['height']) )
	
	if os.path.isfile(path):
		f = open(path,'r')
		content = f.read()
		f.close()
		print( path )
		if content.find( old ) > -1:
			out = open(path,'w')
			out.write( content.replace( old, new ) )
			out.close()
			print( '\t'+"'"+old+"'", '>', "'"+new+"'" )
		else:
			print( '\t'+"'"+old+"'", 'not found' )
	else:
		print( path, 'file NOT found' )

############ INIT

curr_config = copy.deepcopy(config)

############ EDITOR

print_header()
'''
print_config( config )
editor()
print(block_separator)
'''

############ GIT

if curr_config['update_git']:
	
	os.system("git pull")
	
	if not os.path.isdir(GODOT_FOLDER):
		os.makedirs(GODOT_FOLDER)
	
	for version in curr_config['versions']:
		
		if not version['enabled']:
			continue
		
		# preparing godot
		path = os.path.join( GODOT_FOLDER, version['name'] )
		if not os.path.isdir(path):
			os.system("git clone "+version['address']+" "+path)
		os.chdir(path)
		os.system("git fetch "+version['remote'])
		# removing all local changes
		os.system("git checkout --force")
		# updating
		os.system("git pull")
		if version['tag'] != None:
			os.system("git fetch --all --tags")
			os.system("git checkout tags/"+version['tag'] )
		os.chdir(cwd)
		
		# cloning modules
		active_mods = []
		if version['name'] in curr_config['modules']:
			mods = curr_config['modules'][version['name']]
			mod_path = os.path.join( GODOT_FOLDER, version['name'], 'modules' )
			if os.path.isdir(mod_path):
				for mod in mods:
					mp = os.path.join( mod_path, mod['name'] )
					print( 'installing:', mp )
					if not os.path.isdir(mp):
						os.system("git clone "+mod['address']+" "+mp )
					os.chdir( mp )
					# removing all local changes
					os.system("git checkout --force")
					# updating
					os.system("git pull")
					if mod['tag'] != None:
						os.system("git fetch --all --tags")
						os.system("git checkout tags/"+mod['tag'] )
					active_mods.append( mod['name'] )
					os.chdir(cwd)
		
		# removing modules
		for mod in curr_config['modules']['all']:
			if not mod['name'] in active_mods:
				mod_path = os.path.join( GODOT_FOLDER, version['name'], 'modules', mod['name'] )
				if os.path.isdir(mod_path):
					print( 'removing:', mod_path )
					shutil.rmtree(mod_path)
		
		os.chdir(cwd)

############ THEME

for version in curr_config['versions']:
	
	if not version['enabled']:
		continue
	
	vpath = os.path.join( GODOT_FOLDER, version['name'] )
	
	# find & replace
	if 'replace' in version:
		print( '\n# find & replace' )
		for r in version['replace']:
			find_replace( curr_config, os.path.join(path,r[0]), r[1], r[2] )
	
	# images
	print( '\n# assets' )
	tpath = os.path.join( THEME_FOLDER, version['name'] )
	for (dirpath, dirnames, filenames) in os.walk(tpath):
		for f in filenames:
			src = os.path.join( dirpath, f )
			dst = os.path.join( vpath, dirpath[len(tpath)+1:], f )
			out = src + ' > ' + dst
			try:
				shutil.copyfile(src,dst)
				out += ' [ok]'
			except:
				out += ' [FAILED]'
			print(out)
	
	# cleaning cache
	print( '\n# cache' )
	for (dirpath, dirnames, filenames) in os.walk(vpath):
		for f in filenames:
			if f[-6:] == ".gen.h":
				p = os.path.join( dirpath, f )
				try:
					os.remove( p )
					print( p, 'successfully removed' )
				except:
					print( p, 'removal FAILED' )

############ COMPILATION

install_log = open('install.log', 'w')

for version in curr_config['versions']:
	
	if not version['enabled']:
		continue
	
	print( '\n# compilation', version['name'] )
	
	vpath = os.path.join( GODOT_FOLDER, version['name'] )
	os.chdir(path)
	
	for target in curr_config['targets']:
		
		if not target['enabled']:
			continue
		
		if target['clear']:
			os.system( "scons --clean" )
		
		for platform in curr_config['platforms']:
			
			if not platform['enabled']:
				continue 
			
			if platform['clear']:
				os.system( "scons --clean" )
			
			scons_line = "scons -u --jobs="+str(curr_config['jobs'])
			scons_line += " platform="+platform['name']
		
			if platform['llvm']:
				scons_line += " use_llvm=yes"
			if platform['lld']:
				scons_line += " use_lld=yes"
			
			if target['tools']:
				scons_line += " tools=yes"
			else:
				scons_line += " tools=no"

			if target['target'] == 'release':
				scons_line += " debug_symbols=no"
			else:
				scons_line += " debug_symbols=yes"
			
			scons_line += " target="+target['target']
			scons_line += " bits="+str(target['bits'])
			
			print( "\n## starting >", scons_line )
			install_log.write( scons_line + '\n' )
			install_log.flush()
			os.system( scons_line )
	
	compilation_files = os.listdir('bin')
	for cf in compilation_files:
		if cf[:6] == 'godot.':
			os.rename( os.path.join('bin',cf), os.path.join('bin',cf.replace('godot','pozzo') ))
	
	os.chdir(cwd)

install_log.close()
