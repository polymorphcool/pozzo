# POZZO

![banner](https://gitlab.com/polymorphcool/pozzo/-/raw/master/graphics/id/exports/gitlab_banner.png)

**libre game engine for live performances and artgames**

POZZO is 2d/3d general purpose game engine able to communicate with common vjing software and controllers for on-stage visuals.

It is a custom build of [godot](http://godotengine.org) with a redesigned editor and extra modules:

- OSC support
- syphon for mac OSX
- softskin, a soft body library
- futari,  physical particles
- ffmpeg player
- boids 

Download here: [itch.io](https://polymorphcool.itch.io/pozzo)

## devlog

*2021.08.24*: cross-compilation for windows and integration of ffmpeg module

*2021.02.04*: basic theme + linux & windows versions

*2021.02.03*: starting the repo, researches on id and on GUI

## compilation

Pozzo is based on Godot Engine.

Follow instructions available here to prepare your environment:

- [compiling for linux](https://docs.godotengine.org/en/stable/development/compiling/compiling_for_x11.html)
- [cross-compilation for windows](https://docs.godotengine.org/en/stable/development/compiling/compiling_for_windows.html)
- support for mac OSX is not yet done

Once installed, open **install.py** and activate **versions**, **modules**, **versions** and **platforms** 
depending on what you need. The script will clone and prepare the compilation for you.

Once done, just run **install.py**. Executable will appears in **godot/[version]/bin**

### deps for linux mint

```
# godot 
sudo apt install build-essential scons pkg-config libx11-dev libxcursor-dev libxinerama-dev libgl1-mesa-dev libglu-dev libasound2-dev libpulse-dev libudev-dev libxi-dev libxrandr-dev yasm libvdpau-dev

# help linker find opencl
sudo ln -s /usr/lib/x86_64-linux-gnu/libOpenCL.so.1 /usr/lib/libOpenCL.so

# cross-compilation
sudo apt install mingw-w64
sudo update-alternatives --config x86_64-w64-mingw32-gcc
# <choose x86_64-w64-mingw32-gcc-posix from the list>
sudo update-alternatives --config x86_64-w64-mingw32-g++
# <choose x86_64-w64-mingw32-g++-posix from the list>
sudo update-alternatives --config i686-w64-mingw32-gcc
# <choose i686-w64-mingw32-gcc-posix from the list>
sudo update-alternatives --config i686-w64-mingw32-g++
# <choose i686-w64-mingw32-g++-posix from the list>

# ffmpeg
sudo apt-get update -qq && sudo apt-get -y install autoconf automake build-essential cmake git-core libass-dev libfreetype6-dev libgnutls28-dev libsdl2-dev libtool libva-dev libvdpau-dev libvorbis-dev libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev meson ninja-build  pkg-config texinfo wget yasm zlib1g-dev libunistring-dev libaom-dev libdav1d-dev
```
