extends PanelContainer

export var project_path:String = "res://project"

var panels:Array = []
onready var panel_res:Resource = load("res://objects/panel.tscn")
onready var panel_script:Reference = load("res://scripts/panel.gd")
onready var main_viewer:Control = get_node("../background/main_scene")

func register_panel(c:Control) -> void:
	if not c is PanelContainer or c in panels:
		return
	if not c.has_signal("panel_split_up"):
		return
	c.main_viewer = main_viewer
	c.project_path = project_path
	panels.append(c)
	c.connect("panel_split_up",self,"panel_split_up")
	c.connect("panel_split_right",self,"panel_split_right")
	c.connect("panel_split_down",self,"panel_split_down")
	c.connect("panel_split_left",self,"panel_split_left")
	c.connect("panel_close",self,"panel_close")

func unregister_panel(c:Control) -> void:
	if not c is PanelContainer or not c in panels:
		return
	c.disconnect("panel_split_up",self,"panel_split_up")
	c.disconnect("panel_split_right",self,"panel_split_right")
	c.disconnect("panel_split_down",self,"panel_split_down")
	c.disconnect("panel_split_left",self,"panel_split_left")
	c.disconnect("panel_close",self,"panel_close")
	panels.erase(c)

func register_panels(ctrl:Node) -> void:
	register_panel(ctrl)
	for c in ctrl.get_children():
		register_panels(c)

func panel_split_up(panel:Control) -> void:
	panel_split( panel, 0 )

func panel_split_right(panel:Control) -> void:
	panel_split( panel, 1 )

func panel_split_down(panel:Control) -> void:
	panel_split( panel, 2 )

func panel_split_left(panel:Control) -> void:
	panel_split( panel, 3 )

func panel_close(panel:Control) -> void:
	var p = panel.get_parent()
	if p == self:
		return
	p.remove_child(panel)
	unregister_panel(panel)
	if p.get_child_count() == 1:
		var onlyc = p.get_child(0)
		p.remove_child(onlyc)
		var children:Array = []
		var pp = p.get_parent()
		for c in pp.get_children():
			pp.remove_child(c)
			if c == p:
				children.append( onlyc )
			else:
				children.append( c )
		print( children )
		for c in children:
			pp.add_child(c)

func new_vbox() -> VBoxContainer:
	var vbox:VBoxContainer = VBoxContainer.new()
	vbox.size_flags_horizontal = SIZE_EXPAND_FILL
	vbox.size_flags_vertical = SIZE_EXPAND_FILL
	vbox.set("custom_constants/separation",0)
	return vbox

func new_hbox() -> HBoxContainer:
	var hbox:HBoxContainer = HBoxContainer.new()
	hbox.size_flags_horizontal = SIZE_EXPAND_FILL
	hbox.size_flags_vertical = SIZE_EXPAND_FILL
	hbox.set("custom_constants/separation",0)
	return hbox

func adjust_borders(ctrl:Control) -> void:
	
	if not ctrl is VBoxContainer and not ctrl is HBoxContainer:
		return
	
	var bdr:int = 1
	if ctrl is VBoxContainer:
		bdr = 2
	
	for i in range(0,ctrl.get_child_count()):
		var c = ctrl.get_child(i)
		if c is PanelContainer:
			pass

func panel_split(panel:Control, dir:int) -> void:
	
	var newp:Control = panel_res.instance()
	register_panel(newp)
	
	var p = panel.get_parent()
	
	# first split
	if p == self:
		p.remove_child(panel)
		# there is only one panel in editor...
		if dir == 0 or dir == 2:
			p = new_vbox()
		else:
			p = new_hbox()
		add_child( p )
		if dir == 0 or dir == 3:
			p.add_child( newp )
			p.add_child( panel )
		else:
			p.add_child( panel )
			p.add_child( newp )
		newp.copy( panel )
		adjust_borders(p)
		return
	
	var children:Array = []
	# removing all children from parent temporarily
	for c in p.get_children():
		p.remove_child(c)
		children.append(c)
	
	var new_cnt = null
	if p is HBoxContainer and (dir == 0 or dir == 2):
		new_cnt = new_vbox()
	elif p is VBoxContainer and (dir == 1 or dir == 3):
		new_cnt = new_hbox()
	
	if new_cnt != null:
		if dir == 0 or dir == 3:
			new_cnt.add_child( newp )
			new_cnt.add_child( panel )
		else:
			new_cnt.add_child( panel )
			new_cnt.add_child( newp )
		adjust_borders(new_cnt)
	
	# rebuild container
	var firstc = children[0]
	for c in children:
		if c == panel:
			if new_cnt != null:
				p.add_child( new_cnt )
			elif dir == 0 or dir == 3:
				p.add_child( newp )
				p.add_child( panel )
			else:
				p.add_child( panel )
				p.add_child( newp )
		else:
			p.add_child( c )
	
	newp.copy( panel )
	adjust_borders(p)
	
	register_panels(self)

func fit_screen() -> void:
	self.rect_size = get_viewport().size

func _ready() -> void:
	var defp:Control = panel_res.instance()
	defp.main_viewer_enabled = true
	register_panel( defp )
	add_child( defp )
	fit_screen()
	get_viewport().connect("size_changed",self,"fit_screen")

func _process(delta) -> void:
	pass
