extends Spatial

var ctrl_active:bool = false

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT:
		ctrl_active = event.pressed
	elif event is InputEventMouseMotion and ctrl_active:
		rotate_y( event.relative.x * -0.01 )
