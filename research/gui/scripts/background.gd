extends PanelContainer

func fit_screen() -> void:
	self.rect_size = get_viewport().size

func _ready():
	fit_screen()
	get_viewport().connect("size_changed",self,"fit_screen")
