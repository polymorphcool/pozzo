extends VBoxContainer

var project_path:String = '' setget set_project_path

func set_project_path(path:String):
	project_path = path
	var all_files : Array = get_filelist( project_path )
	while get_child_count() > 1:
		remove_child( get_child(get_child_count()-1) )
	
	all_files.sort()
	
	var ppd:int = project_path.split('/').size()
	for f in all_files:
		var ps:Array = f.split('/')
		var entry:Label = $tmpl.duplicate()
		entry.visible = true
		entry.text = ''
		for i in range(ppd,ps.size()-1):
			entry.text += '   '
		entry.text += '|__ '+ps[ps.size()-1]
		add_child(entry)

func get_filelist(scan_dir : String) -> Array:
	var my_files : Array = []
	var dir := Directory.new()
	if dir.open(scan_dir) != OK:
		printerr("Warning: could not open directory: ", scan_dir)
		return []
	if dir.list_dir_begin(true, true) != OK:
		printerr("Warning: could not list contents of: ", scan_dir)
		return []
	var file_name := dir.get_next()
	while file_name != "":
		my_files.append(dir.get_current_dir() + "/" + file_name)
		if dir.current_is_dir():		
			my_files += get_filelist(dir.get_current_dir() + "/" + file_name)
		file_name = dir.get_next()
	return my_files
