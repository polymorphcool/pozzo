extends VBoxContainer

export var background:bool = false

onready var editor_camera:Resource = load("res://objects/editor_camera.tscn")

func _ready():
	$default.visible = background

func load_scene(path:String) -> void:
	
	var res = load(path)
	
	if res == null or not res is PackedScene:
		$vpc.visible = false
		$default.visible = background
		return
	
	while $vpc/viewport.get_child_count() > 0:
		$vpc/viewport.remove_child( $vpc/viewport.get_child(0) )
	
	$vpc/viewport.add_child( res.instance() )
	var ec:Spatial = editor_camera.instance()
	$vpc/viewport.add_child( ec )
	ec.get_child(0).current = true
	$vpc.visible = true
	$default.visible = false

func detach_scene() -> Node:
	if $vpc/viewport.get_child_count() == 0:
		return null
	var sc:Node = $vpc/viewport.get_child(0)
	$vpc/viewport.remove_child(sc)
	return sc

func attach_scene( n:Node ) -> void:
	while $vpc/viewport.get_child_count() > 0:
		$vpc/viewport.remove_child( $vpc/viewport.get_child(0) )
	$vpc/viewport.add_child( n )
