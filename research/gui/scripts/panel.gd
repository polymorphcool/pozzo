extends PanelContainer

signal panel_split_up
signal panel_split_right
signal panel_split_down
signal panel_split_left
signal panel_close
signal take_main_viewer

export var main_viewer_enabled:bool = false

onready var custom_style:StyleBoxFlat = null

var panels:Array = [
	'view',
	'text',
	'image',
	'filesystem',
	'tree',
	'inspector',
	'output',
	'debugger',
	'audio',
	'animation',
	'project',
	'export',
	'settings',
	'help'
]
var type:int = 0
var project_path:String = ''
var scene_editor:Control = null
var text_editor:Control = null
var image_editor:Control = null
var file_list:Array = []
var current_file:String = ''
var main_viewer:Control = null

func _ready() -> void:
	custom_style = get("custom_styles/panel").duplicate()
	$struct/header/type.clear()
	for i in panels:
		$struct/header/type.add_item(i)
	set("custom_styles/panel", custom_style)
	set_type(type)
	$struct/header/type.connect("item_selected",self,"type_changed")
	$struct/header/splitl.connect("pressed",self,"_on_splitl")
	$struct/header/splitu.connect("pressed",self,"_on_splitu")
	$struct/footer/splitd.connect("pressed",self,"_on_splitd")
	$struct/footer/splitr.connect("pressed",self,"_on_splitr")
	
	$struct/header/file.get_popup().connect("index_pressed",self,"file_changed")
	if main_viewer_enabled:
		$struct/header/close.visible = false
	else:
		$struct/header/close.connect("pressed",self,"_on_close")

func copy( src:PanelContainer ) -> void:
	set_type(src.type)

func type_changed(i:int) -> void:
	set_type(i)

func activate_border(i:int) -> void:
	match i:
		-1:
			custom_style.border_width_top = 0
			custom_style.border_width_right = 0
			custom_style.border_width_bottom = 0
			custom_style.border_width_left = 0
		0:
			custom_style.border_width_top = 1
		1:
			custom_style.border_width_right = 1
		2:
			custom_style.border_width_bottom = 1
		3:
			custom_style.border_width_left = 1

func set_type(i:int) -> void:
	
	type = i
	
	$struct/header/type.selected = type
	if type == 0 and main_viewer_enabled:
		custom_style.draw_center = false
		self.mouse_filter = Control.MOUSE_FILTER_IGNORE
		$struct/header.mouse_filter = Control.MOUSE_FILTER_IGNORE
	else:
		custom_style.draw_center = true
		$struct/header.mouse_filter = Control.MOUSE_FILTER_STOP
	$struct/body.mouse_filter = 		$struct/header.mouse_filter
	$struct/body/list.mouse_filter = 	$struct/header.mouse_filter
	$struct/footer.mouse_filter = 		$struct/header.mouse_filter
	
	# cleaning menu
	$struct/header/file.visible = false
	
	# cleaning body
	while $struct/body/list.get_child_count() > 0:
		$struct/body/list.remove_child($struct/body/list.get_child(0))
	
	match type:
		0:
			activate_border(-1)
		_:
			activate_border(1)
			activate_border(2)
	
	match type:
		0:
			build_scene_editor()
		1:
			build_text_editor()
		2:
			build_image_editor()
		3:
			var fs:Control = $struct/filesystem.duplicate()
			$struct/body/list.add_child( fs )
			fs.project_path = project_path
			fs.visible = true

func get_filelist(scan_dir : String) -> Array:
	var my_files : Array = []
	var dir := Directory.new()
	if dir.open(scan_dir) != OK:
		printerr("Warning: could not open directory: ", scan_dir)
		return []
	if dir.list_dir_begin(true, true) != OK:
		printerr("Warning: could not list contents of: ", scan_dir)
		return []
	var file_name := dir.get_next()
	while file_name != "":
		if dir.current_is_dir():
			my_files += get_filelist(dir.get_current_dir() + "/" + file_name)
		else:
			my_files.append(dir.get_current_dir() + "/" + file_name)
		file_name = dir.get_next()
	return my_files

func scan_files(exts:Array) -> void:
	
	file_list = []
	var all_files:Array = get_filelist(project_path)
	for f in all_files:
		for e in exts:
			if f.substr( f.length()-e.length() ) == e:
				file_list.append( f )
				break
	
	file_list.sort()
	
	$struct/header/file.get_popup().clear()
	$struct/header/file.get_popup().add_item( 'none' )
	for f in file_list:
		$struct/header/file.get_popup().add_item( f.replace(project_path,'') )

func file_changed(i:int) -> void:
	current_file = ''
	if i > 0:
		current_file = file_list[i-1]
	match type:
		0:
			if main_viewer_enabled:
				main_viewer.load_scene(current_file)
			else:
				scene_editor.load_scene(current_file)
				custom_style.draw_center = current_file != ''
		1:
			if i == 0:
				text_editor.text = ''
				return
			var f:File = File.new()
			f.open( current_file, File.READ )
			text_editor.text = f.get_as_text()
			f.close()
		2:
			image_editor.load_image(current_file)

func build_scene_editor() -> void:
	
	if not main_viewer_enabled:
		scene_editor = $struct/scene_editor.duplicate()
		$struct/body/list.add_child( scene_editor )
	
	scan_files(['.tscn'])
	file_changed(0)
	
	if not main_viewer_enabled:
		scene_editor.visible = true
	
	$struct/header/file.visible = true

func build_text_editor() -> void:
	text_editor = $struct/text_editor.duplicate()
	$struct/body/list.add_child( text_editor )
	scan_files(['.gd','.shader','.res','.tscn','.txt','.json'])
	file_changed(0)
	text_editor.visible = true
	$struct/header/file.visible = true

func build_image_editor() -> void:
	image_editor = $struct/image_editor.duplicate()
	$struct/body/list.add_child( image_editor )
	scan_files(['.png','.svg','.jpg'])
	file_changed(0)
	image_editor.visible = true
	$struct/header/file.visible = true

func _on_splitl() -> void:
	emit_signal("panel_split_left",self)

func _on_splitu() -> void:
	emit_signal("panel_split_up",self)

func _on_splitd() -> void:
	emit_signal("panel_split_down",self)

func _on_splitr() -> void:
	emit_signal("panel_split_right",self)

func _on_close() -> void:
	emit_signal("panel_close",self)
