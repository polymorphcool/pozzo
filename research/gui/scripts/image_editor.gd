extends HBoxContainer

func reset() -> void:
	$display.texture = null
	$info/name.text = 'none'
	$info/size/width.text = '?'
	$info/size/height.text = '?'

func load_image(path:String) -> void:
	
	reset()
	
	var res = load(path)
	if res == null:
		return
		
	$display.texture = res
	$display.texture.flags = 0
	
	var ps:Array = path.split('/')
	$info/name.text = ps[ps.size()-1]
	$info/size/width.text = str(res.get_width())
	$info/size/height.text = str(res.get_height())
