tool

extends SpotLight

export (bool) var play:bool = true

func _process(delta):
	
	if !play:
		return
	
	var b:Basis = Basis.IDENTITY
	global_transform.basis = global_transform.basis * b.rotated( Vector3.FORWARD, delta )
