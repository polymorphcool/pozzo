#!/usr/bin/env python3

import os, shutil

#### globals

SCRIPT_FOLDER = '../scripts/'
GODOT_FOLDER = '../godot/'
THEME_FOLDER = '../theme/'

#### before starting

# preparation of repos
os.system("git submodule update --recursive --remote")
os.chdir(GODOT_FOLDER)
os.system("git fetch && git checkout 3.3-stable --force")
os.chdir(SCRIPT_FOLDER)

#### theme

print( " ########### THEME ###########" )

for (dirpath, dirnames, filenames) in os.walk(THEME_FOLDER):
	for f in filenames:
		src = os.path.join( dirpath, f )
		dst = os.path.join( GODOT_FOLDER, dirpath[len(THEME_FOLDER):], f )
		out = src + '>>' + dst
		try:
			shutil.copyfile(src,dst)
			out += ' [ok]'
		except:
			out += ' [FAILED]'
		print(out)

# cleaning cache

for (dirpath, dirnames, filenames) in os.walk(GODOT_FOLDER):
	for f in filenames:
		if f[-6:] == ".gen.h":
			p = os.path.join( dirpath, f )
			try:
				os.remove( p )
				print( p, 'successfully removed' )
			except:
				print( p, 'removal FAILED' )

# renaming
rename_list = [
	['version.py', 'short_name = "godot"', 'short_name = "pozzo"'],
	['version.py', 'name = "Godot Engine"', 'name = "POZZO engine"'],
	['version.py', 'website = "https://godotengine.org"', 'website = "https://pozzo.cool"'],
	['platform/windows/godot_res.rc', "Godot Engine", "POZZO engine"],
	['platform/x11/context_gl_x11.cpp', "Godot Engine", "POZZO engine"],
	['editor/editor_themes.cpp', 'preset_accent_color = Color(0.41, 0.61, 0.91);', 'preset_accent_color = Color(0., 1., 0.93);'],
	['editor/editor_themes.cpp', 'preset_base_color = Color(0.2, 0.23, 0.31);', 'preset_base_color = Color(0., 0., 0.);'],
	['editor/editor_fonts.cpp', '(_font_NotoSansUI_Regular, _font_NotoSansUI_Regular_size);', '(_font_Hack_Regular, _font_Hack_Regular_size);'],
	['editor/editor_fonts.cpp', '(_font_NotoSansUI_Bold, _font_NotoSansUI_Bold_size);', '(_font_Hack_Bold, _font_Hack_Bold_size);']
]
for r in rename_list:
	p = os.path.join( GODOT_FOLDER,r[0])
	if os.path.isfile( p ):
		f = open(p,'r')
		content = f.read()
		f.close()
		if content.find( r[1] ) > -1:
			out = open(p,'w')
			out.write( content.replace( r[1], r[2] ) )
			out.close()
			print( r[0], ':', r[1], 'replaced by', r[2] )
		
	else:
		print( r[0], 'file NOT found' )
	
#### compilation

print( " ########### COMPILATION ###########" )


#### osx

'''
os.chdir(GODOT_FOLDER)
os.system( "scons platform=osx arch=x86_64 --jobs=$(sysctl -n hw.logicalcpu)")
os.system( "scons platform=osx arch=arm64 --jobs=$(sysctl -n hw.logicalcpu)")
os.system( "lipo -create bin/godot.osx.tools.x86_64 bin/godot.osx.tools.arm64 -output bin/godot.osx.tools.universal")
quit()
'''

'''
generates all build (cross-compilation)
adapt script to land in folder containing  the repo: https://github.com/godotengine/godot
'''

# ---- linux

# ---- raspbian
'''
$ wget https://gist.githubusercontent.com/silverkorn/f0566819025d4a4882b82b2b27d20826/raw/godot-cross-compile.sh
$ sudo chmod u+x godot-cross-compile.sh
$ sudo ./godot-cross-compile.sh rpi3 raspbian platform=x11 -j 4
'''

# ---- windows 
# install mingw
'''
$ sudo apt install mingw-w64
'''
# -> verify POSIX!
'''
$ sudo update-alternatives --config x86_64-w64-mingw32-gcc
>>> choose x86_64-w64-mingw32-gcc-posix
$ sudo update-alternatives --config x86_64-w64-mingw32-g++
>>>> x86_64-w64-mingw32-g++-posix
'''
# check cmake version
'''
cmake --version
'''
# if it´s < 3.2.3, do << NOT REQUIRED
'''
curl -sSL https://cmake.org/files/v3.14/cmake-3.14.5-Linux-x86_64.tar.gz | sudo tar -xzC /opt
'''
# and patât!

# ---- osx
'''
git clone --depth=1 https://github.com/tpoechtrager/osxcross.git
cd osxcross
'''
# step 1:
# see https://github.com/tpoechtrager/osxcross#packaging-the-sdk
# warning: XCode is ~10G!

'''
>>> download xcode 12.4
sudo apt install clang make liblzma-dev libssl-dev lzma-dev libxml2-dev llvm-dev uuid-dev llvm-dsymutil
sudo tools/get_dependencies.sh

./build_clang.sh

#./tools/gen_sdk_package_pbzx.sh Xcode_12.4.xip

>>> to ensure a correct bacward compatibility, use an old version of xcode (2014)
./tools/gen_sdk_package_p7zip.sh /home/frankiezafe/forge.godot/osxcross/Xcode_6.4.dmg

>>> move MacOSX10.10.sdk.tar.xz to tarballs -> compatibility down to macOS 10.6
./build.sh

cd ../godot_3x/
export OSXCROSS_ROOT="$HOME/forge.godot/osxcross"
'''

os.chdir( os.path.join( os.getcwd(), GODOT_FOLDER ) )
'''
# ---- osx 
## 64 editor
os.system( "scons platform=osx osxcross_sdk=darwin14 -u --jobs=16 tools=yes debug_symbols=yes module_ffmpeg_enabled=no target=release_debug bits=64 --config=force" )
## 64 release_debug
os.system( "scons platform=osx  osxcross_sdk=darwin14 -u --jobs=16 tools=no debug_symbols=no tools=no module_ffmpeg_enabled=no target=release_debug bits=64" )
## 64 release
os.system( "scons platform=osx  osxcross_sdk=darwin14 -u --jobs=16 tools=no debug_symbols=no tools=no module_ffmpeg_enabled=no target=release bits=64" )
'''

# ---- windows 
'''
## 64 editor
os.system( "scons platform=windows -u --jobs=16 tools=yes debug_symbols=yes module_ffmpeg_enabled=no target=release_debug bits=64" )
## 64 release_debug
os.system( "scons platform=windows -u --jobs=16 tools=no debug_symbols=no module_ffmpeg_enabled=no target=release_debug bits=64" )
## 64 release
os.system( "scons platform=windows -u --jobs=16 tools=no debug_symbols=no module_ffmpeg_enabled=no target=release bits=64" )
'''

# ---- linux
## 64 editor
os.system( "scons platform=linuxbsd -u --jobs=16 tools=yes debug_symbols=yes module_ffmpeg_enabled=no target=release_debug bits=64" )
## 64 release_debug
os.system("scons platform=linuxbsd -u --jobs=16 tools=no debug_symbols=yes module_ffmpeg_enabled=no target=release_debug bits=64")
## 64 release
os.system( "scons platform=linuxbsd -u --jobs=16 tools=no debug_symbols=no  module_ffmpeg_enabled=no target=release bits=64" )
'''
## 32 release
#os.system("scons platform=x11 -u --jobs=16 tools=no target=release bits=32")
## 32 release_debug
#os.system("scons platform=x11 -u --jobs=16 tools=no target=release_debug bits=32")
'''

#### packaging

print( " ########### RENAMING ###########" )

'''
for (dirpath, dirnames, filenames) in os.walk(os.path.join( 'bin' )):
	for f in filenames:
		if f[:6] == 'godot.':
			os.rename(os.path.join(dirpath,f), os.path.join(dirpath,'pozzo.'+f[6:]))
'''
